import random
import os




def Match4(): 
    match4board = []
    _list = []


    for i in range (7):
        for j in range (15):
            _list.append(str(" "))
        match4board.append(_list)
        _list = []

    for i in range (6):
        for j in range (0, 16, 2):
            match4board[i][j] = str("|")

    for i in range(1, 15, 2):
        match4board[6][i] = str(i//2 + 1)

    def printMatch4board():
        os.system("clear")
        for i in range(4):
            print("")
        for i in range(7):
            for j in range(15):
                print(match4board[i][j], end = "")
            print("")

            

    def player1Name():
        print("Player 1 - enter name:")
        _player1Name = str(input())
        return _player1Name



    def player2Name():
        print("Player 2 - enter name:")
        _player2Name = str(input())
        return _player2Name

    os.system("clear")
    _player1Name = player1Name()
    _player2Name = player2Name()



    def player1Move():
        print(_player1Name + "'s move:")
        _player1Move = int(input())
        if _player1Move == 1 or _player1Move == 2 or _player1Move == 3 or _player1Move == 4 or _player1Move == 5 or _player1Move == 6 or _player1Move == 7:
            return _player1Move
        else:
            print("Wrong column number!")
            return player1Move()

    def player2Move():
        print(_player2Name + "'s move:")
        _player2Move = int(input())
        if _player2Move == 1 or _player2Move == 2 or _player2Move == 3 or _player2Move == 4 or _player2Move == 5 or _player2Move == 6 or _player2Move == 7:
            return _player2Move
        else:
            print("Wrong column number!")
            return player2Move()

    def append1Pawn():
        columnNumber = player1Move()
        def place1Pawn():
            for rowNumber in range (5, -1, -1):
                if match4board[rowNumber][(columnNumber-1)*2 + 1] == " ":
                    match4board[rowNumber][(columnNumber-1)*2 + 1] = "X"
                    printMatch4board()
                    break
                elif match4board[rowNumber][(columnNumber-1)*2 + 1] != " " and rowNumber == 0:
                    print("This column is full!")
                    append1Pawn()

        place1Pawn()

    def append2Pawn():
        columnNumber = player2Move()
        def place2Pawn():
            for rowNumber in range (5, -1, -1):
                if match4board[rowNumber][(columnNumber-1)*2 + 1] == " ":
                    match4board[rowNumber][(columnNumber-1)*2 + 1] = "O"
                    printMatch4board()
                    break
                elif match4board[rowNumber][(columnNumber-1)*2 + 1] != " " and rowNumber == 0:
                    print("This column is full!")
                    append2Pawn()

        place2Pawn()

    def checkIf1Won():
        for i in range (5, -1, -1):
            for j in range(0, 8, 2):
                if match4board[i][j+1] + match4board[i][j+3] + match4board[i][j+5] + match4board[i][j+7] == "XXXX":
                    print(_player1Name + " won!")
                    return True
        for j in range (1, 15, 2):
            for i in range (0, 3, 1):
                if match4board[i][j] + match4board[i+1][j] + match4board[i+2][j] + match4board[i+3][j] == "XXXX":
                    print(_player1Name + " won!")
                    return True
        if match4board[3][1] + match4board[2][3] + match4board[1][5] + match4board[0][7] == "XXXX":
            print(_player1Name + " won!")
            return True

        if match4board[2][1] + match4board[3][3] + match4board[4][5] + match4board[5][7] == "XXXX":
            print(_player1Name + " won!")
            return True

        if match4board[5][7] + match4board[4][9] + match4board[3][11] + match4board[2][13] == "XXXX":
            print(_player1Name + " won!")
            return True

        if match4board[3][13] + match4board[2][11] + match4board[1][9] + match4board[1][7] == "XXXX":
            print(_player1Name + " won!")
            return True
        
        if "XXXX" in (match4board[4][1] + match4board[3][3] + match4board[2][5] + match4board[1][7] + match4board[0][9]):
            print(_player1Name + " won!")
            return True

        __tempstr = str("")
        for i in range(0, 4, 2):
            for j in range(6):
                __tempstr += match4board[5-j][j*2 + 1 + i]


        if "XXXX" in __tempstr:
            print(_player1Name + " won!")
            return True

        if "XXXX" in (match4board[5][5] + match4board[4][7] + match4board[3][9] + match4board[2][11] + match4board[1][13]):
            print(_player1Name + " won!")
            return True



        if "XXXX" in (match4board[5][9] + match4board[4][7] + match4board[3][5] + match4board[2][3] + match4board[1][1]):
            print(_player1Name + " won!")
            return True

        __tempstr = str("")
        for i in range(2, -2, -2):
            for j in range(5, -1, -1):
                __tempstr += match4board[5-j][j*2 + 1 + i]


        if "XXXX" in __tempstr:
            print(_player1Name + " won!")
            return True

        if "XXXX" in (match4board[4][13] + match4board[3][11] + match4board[2][9] + match4board[1][7] + match4board[0][5]):
            print(_player1Name + " won!")
            return True




    def checkIf2Won():
        for i in range (5, -1, -1):
            for j in range(0, 8, 2):
                if match4board[i][j+1] + match4board[i][j+3] + match4board[i][j+5] + match4board[i][j+7] == "OOOO":
                    print(_player2Name + " won!")
                    return True
        for j in range (1, 15, 2):
            for i in range (0, 3, 1):
                if match4board[i][j] + match4board[i+1][j] + match4board[i+2][j] + match4board[i+3][j] == "OOOO":
                    print(_player2Name + " won!")
                    return True
        if match4board[3][1] + match4board[2][3] + match4board[1][5] + match4board[0][7] == "OOOO":
            print(_player2Name + " won!")
            return True

        if match4board[2][1] + match4board[3][3] + match4board[4][5] + match4board[5][7] == "OOOO":
            print(_player2Name + " won!")
            return True

        if match4board[5][7] + match4board[4][9] + match4board[3][11] + match4board[2][13] == "OOOO":
            print(_player2Name + " won!")
            return True

        if match4board[3][13] + match4board[2][11] + match4board[1][9] + match4board[1][7] == "OOOO":
            print(_player2Name + " won!")
            return True
        
        if "OOOO" in (match4board[4][1] + match4board[3][3] + match4board[2][5] + match4board[1][7] + match4board[0][9]):
            print(_player2Name + " won!")
            return True

        __tempstr = str("")
        for i in range(0, 4, 2):
            for j in range(6):
                __tempstr += match4board[5-j][j*2 + 1 + i]
        print(__tempstr)

        if "OOOO" in __tempstr:
            print(_player2Name + " won!")
            return True

        if "OOOO" in (match4board[5][5] + match4board[4][7] + match4board[3][9] + match4board[2][11] + match4board[1][13]):
            print(_player2Name + " won!")
            return True



        if "OOOO" in (match4board[5][9] + match4board[4][7] + match4board[3][5] + match4board[2][3] + match4board[1][1]):
            print(_player2Name + " won!")
            return True

        __tempstr = str("")
        for i in range(2, -2, -2):
            for j in range(5, -1, -1):
                __tempstr += match4board[5-j][j*2 + 1 + i]
        print(__tempstr)

        if "OOOO" in __tempstr:
            print(_player2Name + " won!")
            return True

        if "OOOO" in (match4board[4][13] + match4board[3][11] + match4board[2][9] + match4board[1][7] + match4board[0][5]):
            print(_player2Name + " won!")
            return True

    def gameFinished():
        print("Press any enter to return to menu...")
        __temp = input()


    __temp_ = 0
    while True:
        if __temp_ >= 42:
            print("Draw! Nobody won!")
            break
        printMatch4board()
        append1Pawn()
        if checkIf1Won() == True:
            break
        append2Pawn()
        if checkIf2Won() == True:
            break
        __temp_ += 1
    gameFinished()












