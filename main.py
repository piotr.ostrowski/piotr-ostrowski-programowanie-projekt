import mastermind
import time
import os
import match4

matrix = []
with open("menu.txt", "r") as menu:
    for line in menu:
        matrix.append(line)



def Menu():
    os.system("clear")
    for i in range(12):
        print(matrix[i], end="")
    print("")

    choice = int(input())
    
    if choice == 0:
        
        print("")
        print("")
        print("")
        print("")

        print("Launching Mastermind, please wait...")

        time.sleep(2)
        mastermind.Mastermind()
        Menu()

    elif choice == 2:
        rulesFile = open("rules.txt", "r")
        print(rulesFile.read())
        print("Press enter key to exit")
        temp = str(input())
        Menu()
    elif choice == 3:


        os.system("cls")

        printHighscores()

        print("")
        print("")
        print("")
        print("")


        print("Press enter key to exit")
        temp = str(input())
        Menu()

    elif choice == 4:
        print("Thanks for playing!")
        time.sleep(3)
        quit
    elif choice == 1:
        
        print("")
        print("")
        print("")
        print("")

        print("Launching Match4, please wait...")

        time.sleep(2)
        match4.Match4()
        Menu()
        
def printHighscores():
    highscores = open("highscores.txt", "r")
    highscoresTable = []

    for line in highscores:
        highscoresTable.append(line.split())


    print("Name", "         ", "Score", "         ", "Time", "         ")
    for row in range(len(highscoresTable)):
        for col in range(3):
            print(highscoresTable[row][col],  "        ", end="")
        print("")





Menu()
    

