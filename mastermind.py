import random
import os
import time
import math

spacer = str("=============================")



def Mastermind():

    def constructBoard():
        board = []
        row = []
        for _ in range(17):
            for _ in range(30):
                row.append(str(" "))
            board.append(row)
            row = []


        for i in range(17):
            if i % 2 == 0:
                board[i][:29] = spacer
            else:
                board[i][:6] = str("Guess ")+str(i//2 + 1)
                board[i][22:29] = str("- - - -")
        return board
    board = constructBoard()

    highscores = open("highscores.txt", "a+")



    def printBoard():
        os.system("clear")
        for i in range(17):
            for j in range(30):
                print(board[i][j], end = "")
            print("")

    def guess(n):
        print("Take your " + str(n+1) + " guess:")
        shot = str(input())
        if len(shot) != 4:
            print("Your guess must be 4 numbers!")
            return guess(n)
        else:
            return shot

    def guessedNumber():
        result = ""
        while True:
            a = str(random.randint(0,9))
            if a not in result:
                result = result + str(a)
            if len(result) == 4:
                return result
                break

    def enterNickname(nickname):
        return nickname


    def gameWon(score, time):
        print("Enter your name:")
        name = enterNickname(str(input()))
        newScore = [name, score, time+"s"]
        for i in newScore:
            highscores.write(i)
            highscores.write(" ")
        highscores.write("\n")
        


        
        print("Press any enter to return to menu...")
        _temp = input()



    def printShots():
        now = time.time()
        _guessedNumber = guessedNumber()
        for Round in range(0, 16, 2):

            shot = guess(Round//2)
            board[Round + 1][12:16] = str(shot)
        
            for x in range(4):
                if shot[x] == _guessedNumber[x]:
                    board[Round + 1][22 + x*2] = "+"
            printBoard()

            if shot == _guessedNumber:
                timeScoreStr = str(time.time()-now)[:-13]
                timeScoreFloat = float(timeScoreStr)
                pointsScore = str(math.modf(pow(timeScoreFloat, -1)*1000000)[1])
                print("Congratulations! You won in " + str(Round//2 + 1) + "th guess!")
                print("Your time is:  " + timeScoreStr + "s")
                print("You scored " + str(math.modf(pow(timeScoreFloat, -1)*1000000)[1])  + " points!")
                """ miejsce na highscore """
                gameWon(str(math.modf(pow(timeScoreFloat, -1)*1000000)[1]), timeScoreStr)
                break

            if (Round//2 + 1) == 8 and shot != _guessedNumber:
                timeScoreStr = str(time.time()-now)[:-13]
                timeScoreFloat = float(timeScoreStr)
                print("You lost... Better luck next time!")
                print("Your time is:  " + timeScoreStr)
                print("Press enter key to return...")
                __temp = input()

    printBoard()
    printShots()
